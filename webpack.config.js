const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: './index.js'
  },
  devtool: 'inline-source-map', //Enables easy debugging by mapping errors to raw file
  devServer: { //Enables development server in http://localhost:8081/ with auto reload
    contentBase: './dist',
    hot: true //Enables HMR
  },
  plugins: [
    new CleanWebpackPlugin(['dist']), //Deletes old files in 'dist' folder
    new HtmlWebpackPlugin({ //Enables the dynamic creation of dist/index.html
      title: 'NG-UI-Widgets by Macmillan Learning'
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      { //This enables you to import './style.css' into the file that depends on that styling.
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      { //This enables importing image files: import MyImage from './my-image.png'
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      },
      { //This enables importing fonts from the css files
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  }
};
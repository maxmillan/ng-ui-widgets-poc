## The Manifesto 

NG-UI-Widgets is a project intended to ship the *best* components for React application in Macmillan Learning. It is focused on the following principles:

- **Quality over quantity**: we rather develop a bunch components close to perfection than a lot of unmaintainable buggy components.
- **Pixel perfection**: every component should be nailed to the spec. We will pay attention to every single detail at both visual and interaction levels.
- **Flexibility**: components should be as decoupled as possible from opinionated styling libraries/tools. They should be easy to theme and customize.
- **Easy adoption**: the project should be integrated as easy as technology permits. 
- **Exhaustive testing**: every component should be intensively tested.
